#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 23 18:21:03 2020
Ce programme traite les données d'un fichier csv obtenue sur le site du
gouvernement qui contient toutes les bornes de recharge de voiture éléctrique
publiques en France. Les DOM TOM présent seront retirés et les données seront
aggremantées par les adresse de chaque borne
@author: alexis
"""
from urllib.request import urlopen
import json
import time
import pandas as pd
from bs4 import BeautifulSoup
import datetime
from openpyxl import load_workbook
# from geopy.geocoders import Nominatim
from peewee_borne import db,Utilisateurs,Contacts,Authentification
from werkzeug.security import generate_password_hash

def traitement_csv():
    csv = "/Users/alexis/Desktop/projet_bornes/projetpersoAlexis/"
    csv += "charging_station_csv/data.csv"
    df = pd.read_csv(csv, engine='python', sep=";")
    #observation graphique des données
    df.plot.scatter('X', 'Y')
    #supression de toutes les données indésirables
    new_df = df[['X', 'Y', 'accessibilité', 'n_operateur', 'acces_recharge',
                 'n_amenageur']]
    new_df = new_df[(new_df['X'] < 40)&(new_df['X'] > -40)].reset_index(drop=True)
    #verification graphique
    new_df.plot.scatter('X', 'Y')
    #transformation de la dataframe en liste de dictionnaire
    liste_borne = new_df.to_dict('records')
    return liste_borne


def recuperation_localisation(dico, liste_imcomplete):
    """recuperation des adresses des bornes de recharges graçe a leurs
    coordonnée géographique, la borne est entrée sous la forme d'un
    dictionnaire est doit contenir les clés X,Y,n_amenageur au minimum"""

    url = "https://api-adresse.data.gouv.fr/reverse/?lon="
    url += str(dico['X'])+"&lat=" +str(dico['Y'])
    html = urlopen(url)
    time.sleep(0.2)
    soup = BeautifulSoup(html, "html.parser")
    j = json.loads(soup.text)

    if len(j['features']) < 1:
        try:
            if 'Paris' in dico['n_amenageur']:
                dico['ville'] = 'Paris'
                dico['adresse'] = dico['n_amenageur']

            else:
                liste_imcomplete += [dico]
                print('imcomplet')
        except:
            liste_imcomplete += [dico]
            print('imcomplet2')

    else:
        dico['adresse'] = j['features'][0]['properties']['label']
        dico['ville'] = j['features'][0]['properties']['city']

    return dico


def calcul_dataframe(X):
    return lambda x: x - X

def calcul_dataframeY(Y):
    return lambda x: x-Y

# données_imcomplete.to_excel("données_imcomplete.xlsx")
def sauvetage_erreur(liste_imcomplete, liste_borne):
    """ recupération des villes des données qui n'ont pas pus être scrapper
    en fonction de leurs proximité avec les bornes qui ont une ville assigné
    """
    donnees_complete = pd.DataFrame(liste_borne)
    donnees_imcomplete = pd.DataFrame(liste_imcomplete)

    for i in range(0, len(donnees_imcomplete)):
        X = donnees_imcomplete['X'].loc[i]
        Y = donnees_imcomplete['Y'].loc[i]
        df_calcul = donnees_complete.copy()
        df_calcul['calculX'] = df_calcul['X']
        df_calcul['calculY'] = df_calcul['Y']
        df_calcul['calculX'] = df_calcul['calculX'].apply(calcul_dataframe(X))
        df_calcul['calculY'] = df_calcul['calculY'].apply(calcul_dataframeY(Y))

        df_calcul = df_calcul[(df_calcul['calculX'] < 0.1)&(
            df_calcul['calculX'] > -0.01)]
        df_calcul = df_calcul[(df_calcul['calculY'] < 0.1)&(
            df_calcul['calculY'] > -0.01)]

        if len(df_calcul) > 1:
            decompte = df_calcul['ville'].value_counts()
            if decompte[0]/len(df_calcul)*100 > 50:
                liste_imcomplete[i]['ville'] = decompte.index[0]

def mise_en_forme(borne):
    """mise en forme des dictionnaires contenant les borne principales
    du programmes pour qu'ils correspondent au format de la base de donnée
    postgres """
    cles_neccessaire = ['n_operateur', 'accessibilité', 'acces_recharge',
                        'adresse', 'ville']
    format_correct = {'X':'long', 'Y':'lat', 'n_operateur':'operateur',
                      'acces_recharge':'service'}

    for cle in cles_neccessaire:
        if cle not in borne.keys():
            borne[cle] = 'inconnue'

    for key, value in format_correct.items():
        if key in borne.keys():
            borne[value] = borne.pop(key)


    for colonne in borne.keys():
        if str(borne[colonne]) == 'nan':
                borne[colonne] = 'inconnue'


    return borne

# def recuperation_adresse():
#     """refonte possible du code """
#     geolocateur = Nominatim(user_agent ='alexis')
#     bornes = Bornes.select().where(Bornes.adresse=='inconnue')
#     villes = Villes.select()
#     for borne in bornes:
#         location = geolocateur.reverse(str(borne.lat)+","+str(borne.long))
        
#         borne.ville = location.raw['address']['municipality']
#         borne.adresse = location.address
def recup_date():
    now = datetime.datetime.now()
    str_now = now.strftime("%Y-%m-%d-%H-%M-%S")
    return str_now

def creation_metadonnees():
    """ cette fonction crée un repertoire de metadonnées sous format excel 
    aprés l'insertion des premiéres données dans la base """
    nom_tables = []
    liste = []
    
    tables = db.execute_sql("""SELECT table_name
                            FROM information_schema.tables
                            WHERE table_schema='public'
                            AND table_type='BASE TABLE';""")
                            
    for table in tables:
        nom_tables += [table[0]]
    
    
    # chaque table de la base de données est sauvegardée sous forme d'un 
    # fichier Excel  
    for table in nom_tables:
        
        
        # Date actuelle 
        now = datetime.datetime.now()
        # date actuelle en chaine de carac
        str_now = now.strftime("%Y-%m-%d-%H-%M-%S")
        
        # onglet prend la valeur d'index 0 du tuple (le nom des tables)
      
        # on ajoute à la feuille excel la valeur de "onglet", 1ere itération
        # onglet : connaissances

        # on récupére toutes les données de "onglet"
        requete_SQL = "SELECT * FROM " +str(table)
        test = db.execute_sql(requete_SQL)
        for t in test :
           
            liste += [{'type':'insert','table':table,'id':t[0],'date':str_now,
                       'utilisateur':'admin'}]
        
    df = pd.DataFrame(liste)
    print(df)

    nom_fichier = "meta.xlsx"

    with pd.ExcelWriter(nom_fichier) as writer:
        
        df.to_excel(writer, sheet_name='metadonnées',index=False )
        print(df.head())
        print("C'est sauvegardé à "+str_now+" dans "+nom_fichier)

def modif_meta(nouvelles_lignes):
    wb = load_workbook("meta.xlsx")
    onglet = wb.worksheets[0]
    for ligne in nouvelles_lignes:
        onglet.append(ligne)
    wb.save("meta.xlsx")
    
def select_modif2(user,tel,email,new_pass,new_pass2):
    str_now = recup_date()
    message = 'vos modifications ont étés enregistrez'
    
    if tel != "":
        contact= Contacts.select().where(Contacts.id_utilisateur
                                == user.id_utilisateur)
        if len(contact) == 0:
            ajout = Contacts.create(id_utilisateur=user.id_utilisateur, 
                            contact=str(tel))
            utilisateur  = (Utilisateurs.select(Utilisateurs,Authentification)
            .join(Authentification, on=(Utilisateurs.id_utilisateur == 
                                  Authentification.id_utilisateur))
           .where(Utilisateurs.pseudo==user.pseudo)[0])
            modif_meta([['insert','contacts',ajout.id_contact,str_now,
                         'web_profil']])
        else:   
            utilisateur = (Utilisateurs.select(Utilisateurs,
                                                   Authentification,Contacts)
                    
                    .join(Authentification, on=(Utilisateurs.id_utilisateur == 
                                       Authentification.id_utilisateur)).join(
                                Contacts, on=(Utilisateurs.id_utilisateur == 
                                                    Contacts.id_utilisateur))
                    .where(Utilisateurs.pseudo==user.pseudo)[0])
                                           
            utilisateur.authentification.contacts.contact = str(tel)
            modif_meta([['update','contacts',
                       utilisateur.authentification.contacts.id_contact,
                       str_now,'web_profil']])
            utilisateur.authentification.contacts.save()
    else:
        utilisateur  = (Utilisateurs.select(Utilisateurs,Authentification)
         .join(Authentification, on=(Utilisateurs.id_utilisateur == 
                                  Authentification.id_utilisateur))
         .where(Utilisateurs.pseudo==user.pseudo)[0])
         
    if email != "":
        utilisateur.mail = email
        modif_meta([['update','utilisateurs',utilisateur.id_utilisateur,
                     str_now,'web_profil']])
        utilisateur.save()
        
    if new_pass != "":
        if new_pass == new_pass2:
            utilisateur.authentification.mot_de_passe =  (
                generate_password_hash(new_pass))
            modif_meta([['update','authentifications',
                         utilisateur.authentification.id_auth,
                     str_now,'web_profil']])
            utilisateur.authentification.save()
            
        else:
            message = 'les mots de passe ne sont pas identiques'
            
    return message

def verif_contact(id_user):
    contact= Contacts.select().where(Contacts.id_utilisateur
                                == id_user)
    if len(contact) == 0:
        contact_existant = False
    else:
        contact_existant = True
        
    return contact_existant

def verif_borne(id_user):
    contact= Contacts.select().where(Contacts.id_utilisateur
                                == id_user)
    if len(contact) == 0:
        borne_existante = False
    else:
        borne = contact[0].id_borne
        if borne == None:
            borne_existante = False
        else: 
            borne_existante = True
        
    return borne_existante
    
def recup_adresse(adresse):
    adresse = adresse.replace(' ','+')
    adresse += '&limit=1'
    url = 'https://api-adresse.data.gouv.fr/search/?q='+adresse
    html = urlopen(url)
    time.sleep(0.2)
    soup = BeautifulSoup(html, "html.parser")
    j = json.loads(soup.text)
    x = j['features'][0]['geometry']['coordinates'][1]
    y = j['features'][0]['geometry']['coordinates'][0]
    return x,y

if __name__ == '__main__':
    lancement = False
    if lancement:
        liste_borne = traitement_csv()
        liste_imcomplete = []
        for dico in liste_borne:
            dico = recuperation_localisation(dico, liste_imcomplete)
        for dico2 in liste_imcomplete:
            liste_borne.remove(dico2)
        sauvetage_erreur(liste_imcomplete, liste_borne)
        for borne in liste_borne:
            borne = mise_en_forme(borne)
        for borne2 in liste_imcomplete:
            borne2 = mise_en_forme(borne2)

        liste_borne = pd.DataFrame(liste_borne+liste_imcomplete)
        liste_borne = liste_borne.applymap(
                            lambda x: str(x).replace("'", "''"
                                                     )).to_dict('records')
        for d in liste_borne:
            try:
                """ integration des donnée dans la base avec un appel de la 
                procédure stockée insertion crée sur pg admin l'interface 
                graphique de postgres"""
                
                query = "call insertion('"+d['ville']+"','"+d['operateur']+"','"
                query += d['accessibilité']+"', 'publique' ,'"+d['service']+"',"
                query += str(d['lat'])+","+str(d['long'])+",'"+d['adresse']+"')"       
                db.execute_sql(query)

            except Exception as e:
                print(e)
                
        # création d'un administrateur celui_ci devrait modifier ses infos 
        admin = Utilisateurs.create(id_ville=1,mail='admin@admin',
                                   pseudo='admin_web' ,nom='admin',
                                   prenom='admin',accreditation=2)
        Authentification.create(id_utilisateur=admin.id_utilisateur,
                                mot_de_passe='Administrateur1')
        
        
