#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Structue de la base de donnée neccéssaire au fonctionnement de l'application,
ce code permet la création des tables qui seront contenues dans la base de
donnée postgres. Celle-ci est divisée en 9 tables differentes qui peuvent être
reliées entre elles grâce aux differents id. Par exemple la table utilisateurs
et la table Authentification se rejoingne grâce a l'id utilisateur
Created on Tue Jul 28 21:35:46 2020
@author: alexis
"""

from peewee import Model
from peewee import (AutoField, DateField, TextField, IntegerField,
                    CharField, ForeignKeyField)
from playhouse.postgres_ext import PostgresqlExtDatabase, ArrayField, FloatField
from flask_login import UserMixin



# un fichier contient les instructions de connexion a la base de donnée
fichier = open("/Users/alexis/Desktop/projet_bornes/projetpersoAlexis"+
               "/test_fichier.rtf", "r")

lignes = fichier.readlines()
liste = []

for ligne in lignes:
    if ligne != '\\\n':
        lignepropre = ligne.replace("\\", "")
        lignepropre = lignepropre.replace("\n", "")
        lignepropre = lignepropre.replace("}", "")
        liste += [lignepropre]

#aprés lecture de ce fichier une connexion est initialisé
db = PostgresqlExtDatabase(host=liste[9],
                           database=liste[10],
                           user=liste[11],
                           password=liste[12],
                           port=5432)

class BaseModel(Model):
    """ l'utilisation du module peewee permet de représenté les tables
     sous formes de classes, cela permet d'effectuer des requête sql de façon
     intuitive sans s'incombrer  du language SQL"""

    class Meta:
        database = db

#UserMixin est un héritage Flask_login
class Utilisateurs(BaseModel, UserMixin):
    """ Pour instancier une table avec peewee il faut procéder comme suit:
        nom de la colonne = type de la donnée. le nom de la classe correspond
        au nom de la table"""
        
    id_utilisateur = AutoField()
    id_ville = IntegerField()
    mail = TextField(unique=True)
    pseudo = TextField(unique=True)
    nom = TextField()
    prenom = TextField()
    accreditation = IntegerField()


    def __repr__(self):
        """ representation d'un utilisateur sous un format unicode,
        exemple pour un utilisateur test qui à un id 1 et un mail
        test@test.fr sa représentation sera:
            1/test/test@test.fr . L'unicode sera utilisé pour recharger la
            connexion de l'utilisateur dans l'app web. """
            
        return "%d/%s/%s" % (self.id_utilisateur, self.pseudo, self.mail)

class Bornes(BaseModel):
    id_borne = AutoField()
    id_ville = IntegerField()
    id_operateur = IntegerField()
    id_access = IntegerField()
    id_type = IntegerField()
    service = TextField()
    lat = FloatField()
    long = FloatField()
    adresse = TextField()

class Authentification(BaseModel):
    id_auth = AutoField()
    id_utilisateur = IntegerField()
    mot_de_passe = TextField()


class Operateurs(BaseModel):
    id_operateur = AutoField()
    nom_operateur = TextField()

class Villes(BaseModel):
    id_ville = AutoField()
    nom_ville = TextField()

class Contacts(BaseModel):
    id_contact = AutoField()
    id_borne = IntegerField(null=True)
    id_utilisateur = IntegerField()
    contact = TextField()

class Accessibilite(BaseModel):
    id_access = AutoField()
    horraire = TextField()

class Types_bornes(BaseModel):
    id_type = AutoField()
    type_borne = TextField()
    
class Bornes_imcompletes(BaseModel):
    id_incomplet = AutoField()
    id_borne = IntegerField()
    id_utilisateurs = ArrayField(null=True)
    com_ville = ArrayField(null=True)
    com_adresse = ArrayField(null=True)
    statut = TextField()

def chargement_utilisateur(pseudo=None, id_utilisateur=None):
    """Retourne un utilisateur par son pseudo ou son id, retourne None si
    le chargement a échoué. Nous retrouvons ici l'unicode de la representation
    d'un utilisateur
    """
    if pseudo:
        for utilisateur in (Utilisateurs.select()
                            .where(Utilisateurs.pseudo == pseudo)):
            return utilisateur

    if id_utilisateur:
        for utilisateur in (Utilisateurs.select()
                            .where(Utilisateurs.id_utilisateur ==
                                   id_utilisateur)):
            return utilisateur

    return None

# def select_modif(user,tel,email,new_pass,new_pass2):
    
#     message = 'vos modifications ont étés enregistrez'
    
#     if tel != "":
#         contact= Contacts.select().where(Contacts.id_utilisateur
#                             == user.id_utilisateur)
#         if len(contact) == 0:
#             Contacts.create(id_utilisateur=user.id_utilisateur,contact=str(tel))
    
#         else:
#             contact[0].contact = str(tel)
#             contact[0].save()
            
#     if email != "":
#         utilisateur = Utilisateurs.select().where(Utilisateurs.id_utilisateur
#                                                   == user.id_utilisateur)
#         utilisateur[0].mail = email
#         utilisateur[0].save()
        
#     if new_pass != "":
#         if new_pass == new_pass2:
#             authentification = Authentification.select().where(Authentification.
#                                                                id_utilisateur
#                                                   == user.id_utilisateur)
#             authentification[0].mot_de_passe = (
#                 generate_password_hash(new_pass))
#             authentification[0].save()
#         else:
#             message = 'les mots de passe ne sont pas identiques'
            
            
#     return message

                  

def select_villes():
    """ Cette fonction parcours la base de données et ressorts tout les noms
    de villes dans une liste, elle est utilisée pour l'aucompletion de 
    l'application web ce qui éviteras les doublons pour faute de syntaxe"""
    
    villes=[]
    villes_db = Villes.select()
    for ville in villes_db :
        villes += [ville.nom_ville] 
        
    return villes
   
if __name__ == '__main__':

    creation_table = False


    if creation_table:

        
        db.drop_tables([Utilisateurs,Bornes,Authentification,Operateurs,Villes
                        ,Contacts,Accessibilite,Types_bornes,
                        Bornes_imcompletes])
        
        db.create_tables([Utilisateurs,Bornes,Authentification,Operateurs,
                          Villes,Contacts,Accessibilite,Types_bornes,
                          Bornes_imcompletes])
