#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 23 18:21:03 2020

@author: alexis
"""

import matplotlib.pyplot as plt
import pandas as pd
import geopandas as gpd
import folium
import json 

df = pd.read_csv("/Users/alexis/Desktop/projet bornes/charging_station_csv/data.csv",
                 engine='python',sep=";")

df2 = gpd.read_file('/Users/alexis/Desktop/projet bornes/charging_station_geojson/data.geojson')



fig, ax = plt.subplots()

df2.plot(color='gray', ax=ax)
df2.plot(column='n_amenageur', cmap='Blues', linewidth=0.8, edgecolor='1.5',ax=ax, legend=False)

snow_map = folium.Map(location=[45,1], zoom_start=5)
snow_map.choropleth(
   geo_data='/Users/alexis/Desktop/projet bornes/charging_station_geojson/data.geojson', # geoJSON file
   name='choropleth',
   data=df, # Pandas dataframe
   columns=['X','Y'], # key and value of interest from the dataframe
   #key_on='feature.properties.index', # key to link the json file and the dataframe
   fill_color='Blues', # colormap
   fill_opacity=0.9,
   line_opacity=0.2,
   legend_name='Hauteur moyenne de neige (m)'
)

snow_map.save('carte.html')

c= folium.Map(location=[49.0055,2.6056],zoom_start=5)
for i in range(len(df)-1) : 
    folium.Marker([df.loc[i]['Y'],df.loc[i]['X']],popup='essaie').add_to(c)
c.save('carte.html')


html = urlopen(url)
soup = BeautifulSoup(html,"html.parser")    

url="https://api-adresse.data.gouv.fr/reverse/?lon=2.583&lat=48.838"