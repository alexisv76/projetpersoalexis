#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 11:49:13 2020

@author: alexis
"""


from flask import (Flask, render_template, redirect, url_for, flash, session,
                   request, Response)

from flask_login import (LoginManager, UserMixin,
                         login_required, login_user, logout_user, current_user)

from peewee_borne import (Villes,chargement_utilisateur,Utilisateurs,
                          Authentification,Bornes,Contacts,
                          select_villes)
from werkzeug.security import generate_password_hash, check_password_hash
import pandas as pd
from traitement import modif_meta, recup_date,select_modif2

app = Flask(__name__)
app.config.update(
    DEBUG=False,
    SECRET_KEY='secret_xxx')

login_manager = LoginManager()
login_manager.login_view = "login"
login_manager.init_app(app)

@app.route('/')
def accueil():
    villes = select_villes()
    title = "Accueil"
    return render_template("accueil.html",villes=villes, title=title)

@app.route('/carte', methods=['POST','GET'] )
def carte():
   if request.method == 'POST':
        ville = request.form['ville']
        bornes = Bornes.select(Bornes,Villes).join(Villes, on =(Bornes.id_ville ==
                            Villes.id_ville)).where(Villes.nom_ville==ville)
        
   else:
        bornes = Bornes.select(Bornes,Villes).join(Villes, on =(Bornes.id_ville ==
                            Villes.id_ville))
   bornes = list(bornes.dicts())
   return render_template('carte.html',bornes=bornes)

@app.route('/renseignement')
def renseignement():
    message = None
    return render_template('renseignement.html',message=message)

@app.route('/modif_profil',methods=['POST'])
def modif_profil():
    tel =request.form['telephone']  
    email = request.form['mail']
    new_pass = request.form['pass']
    new_pass2 = request.form['pass2']
    message = select_modif2(current_user,tel,email,new_pass,new_pass2)
 
    
    return render_template('renseignement.html', message=message)
    
    

@app.route('/form_auth')
def form_auth():
    return render_template('signin.html')

@app.route('/connexion', methods=['POST','GET'])
def connexion():
    
    username = request.form['pseudo']
    
    
    utilisateur  = (Utilisateurs.select(Utilisateurs,Authentification)
         .join(Authentification, on=(Utilisateurs.id_utilisateur == 
                                  Authentification.id_utilisateur))
         .where(Utilisateurs.pseudo==username))
    if check_password_hash(utilisateur[0].authentification.mot_de_passe,
                                request.form['pass']) :
    
        user = chargement_utilisateur(pseudo=username)
        
        login_user(user)
        villes = select_villes()
        return render_template('accueil.html',villes=villes)
    else:
        return render_template('signin.html')
    
@app.route('/test_connexion')
def test_connexion() :
    print("-"*10)
    print(current_user)
    print(type(current_user))
    print(current_user.pseudo)

    print("-"*10)
    return redirect("/form_auth")

@app.route('/form_inscription')
def form_inscritpion():
    villes = select_villes()
   
    return render_template('signup.html',villes=villes)

@app.route('/traitement_auth',methods=["POST"])
def traitement_auth():
    prenom = request.form['prenom']
    email = request.form['email']
    nom = request.form['nom']
    pseudo = request.form['pseudo']
    ville_saisit = request.form['ville']
    str_now = recup_date()

    #Verification que les mdp sont les même
    if request.form['pass'] != request.form['pass2'] :
        return redirect("/traitement_auth")

    try :
        mot_de_passe_sale = generate_password_hash(
                                            request.form['pass'])
        ville = Villes.select().where(Villes.nom_ville==ville_saisit)
        if len(ville) == 0:
            ville = Villes.create(nom_ville=ville_saisit)
            id_vill = ville.id_ville
        else:
            id_vill = ville[0].id_ville
        
        user=Utilisateurs.create(id_ville=id_vill,mail=email,pseudo=pseudo,
                            nom=nom,prenom=prenom,accreditation=1)
        auth = Authentification.create(id_utilisateur=user.id_utilisateur,mot_de_passe
                                =mot_de_passe_sale)
        modif_meta([['insert','utilisateurs',user.id_utilisateur,str_now,
                     'web_inscritpion'],['insert','authenthification',
                                         auth.id_auth,str_now,
                                         'web_inscription']])
        login_user(user)
        villes = select_villes()
        
        return render_template('accueil.html',villes=villes)
    
    except Exception as e:
        print(e)
        
        return "/traitement_auth"
    
@app.route("/logout")
@login_required
def logout():
    logout_user()
    villes = select_villes()
    
    return render_template('accueil.html',villes=villes)

@login_manager.user_loader
def load_user(userid):
    utilisateur = chargement_utilisateur(id_utilisateur=userid)
    return utilisateur

    
    
if __name__ == '__main__':
    app.run(debug=True, use_reloader=False)
    