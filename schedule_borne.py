#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 19:11:39 2020
Ce fichier contient deux fonctions, la premiére permet de sauvegarder 
l'esemble de la base de donnée et la seconde permet de restaurer les 
données en cas de problémes. Un planificateur permet de lancer une sauvegarde
toutes les 24 heures'
@author: alexis
"""
from apscheduler.schedulers.background import BackgroundScheduler
import os
import datetime

def sauvegarde():
    """fonction de sauvegarde de la base, les export permettent de définir un 
    environnement à l'os systeme puis la commande pg_dump permet d'effectuer 
    la sauvegarde, il'agit de la commande intégrée postgres"""
    
    jour = str(datetime.datetime.now()).replace(" ","-")
    
    os.system("""export PATH=/usr/local/Cellar/postgresql/12.4/bin:$PATh;
              export PGDATA=/Library/PostgreSQL/12/data;
              export PGDATABASE=postgres;
              export PGUSER=postgres;
              export PGPASSWORD="0904";
              export PGLOCALEDIR=/Library/PostgreSQL/12/share/locale;
              export MANPATH=$MANPATH:/Library/PostgreSQL/12/share/man;
              pg_dump projet_bornes > base_"""+jour+".sql")   
          


def backup(chemin_du_fichier):
    """Même principe que la fonction sauvegarde sauf qu'ici la commande psql
    permet une restauration des données."""
    
    os.system("""export PATH=/usr/local/Cellar/postgresql/12.4/bin:$PATh;
              export PGDATA=/Library/PostgreSQL/12/data;
              export PGDATABASE=postgres;
              export PGUSER=postgres;
              export PGPASSWORD="0904";
              export PGLOCALEDIR=/Library/PostgreSQL/12/share/locale;
              export MANPATH=$MANPATH:/Library/PostgreSQL/12/share/man;
              psql projet_bornes < """+chemin_du_fichier)


if __name__ == '__main__':

    sauvegarde_auto = False
    
    if sauvegarde_auto:
        """ BackgroundScheduler permet de lancer une fonction en arriére 
        plan, il s'agit d'un planificateur qui ici sauvegarde les données"""
        
        sched = BackgroundScheduler(daemon=True)
        sched.add_job(sauvegarde,'interval',hours=24)
        sched.start()