#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from peewee import Model
from peewee import (AutoField, DateField, TextField, IntegerField,
                    CharField, ForeignKeyField,fn)
from playhouse.postgres_ext import PostgresqlExtDatabase, ArrayField, FloatField
from flask_login import UserMixin
from werkzeug.security import generate_password_hash
import pandas as pd
import time
import matplotlib.pyplot as plt
# un fichier contient les instructions de connexion a la base de donnée
"""
resultat pour l'insertion avec procédure stockée
La fonction met 0.007823944091796875 seconde a tourné
            pour 3
La fonction  met 0.015378952026367188 seconde a tourné
            pour 30
La fonction  met 0.1676349639892578 seconde a tourné
            pour 330
La fonction  met 1.718822956085205 seconde a tourné
            pour 3330
La fonction  met 3.509577989578247 seconde a tourné
      pour 5291
      
resultat sans procédure 
La fonction  met 0.015128135681152344 seconde a tourné
            pour 3
La fonction  met 0.10552310943603516 seconde a tourné
            pour 30
La fonction met 0.8590762615203857 seconde a tourné
            pour 330
La fonction  met 8.329765796661377 seconde a tourné
            pour 3330
La fonction addition met 15.033708095550537 seconde a tourné
         pour 5291"""
            

        
fichier = open("/Users/alexis/Desktop/projet_bornes/projetpersoAlexis"+
               "/test_fichier.rtf", "r")

lignes = fichier.readlines()
liste = []

for ligne in lignes:
    if ligne != '\\\n':
        lignepropre = ligne.replace("\\", "")
        lignepropre = lignepropre.replace("\n", "")
        lignepropre = lignepropre.replace("}", "")
        liste += [lignepropre]

#aprés lecture de ce fichier une connexion est initialisé
db = PostgresqlExtDatabase(host=liste[9],
                           database= 'postgres',
                           user=liste[11],
                           password=liste[12],
                           port=5432)

class BaseModel(Model):
    """ l'utilisation du module peewee permet de représenté les tables
     sous formes de classes, cela permet d'effectuer des requête sql de façon
     intuitive sans s'incombrer  du language SQL"""

    class Meta:
        database = db

#UserMixin est un héritage Flask_login
class Utilisateurs(BaseModel, UserMixin):
    """ Pour instancier une table avec peewee il faut procéder comme suit:
        nom de la colonne = type de la donnée. le nom de la classe correspond
        au nom de la table"""
        
    id_utilisateur = AutoField()
    id_ville = IntegerField()
    mail = TextField(unique=True)
    pseudo = TextField(unique=True)
    nom = TextField()
    prenom = TextField()
    accreditation = IntegerField()


    def __repr__(self):
        """ representation d'un utilisateur sous un format unicode,
        exemple pour un utilisateur test qui à un id 1 et un mail
        test@test.fr sa représentation sera:
            1/test/test@test.fr . L'unicode sera utilisé pour recharger la
            connexion de l'utilisateur dans l'app web. """
            
        return "%d/%s/%s" % (self.id_utilisateur, self.pseudo, self.mail)

class Bornes(BaseModel):
    id_borne = AutoField()
    id_ville = IntegerField()
    id_operateur = IntegerField()
    id_access = IntegerField()
    id_type = IntegerField()
    service = TextField()
    lat = FloatField()
    long = FloatField()
    adresse = TextField()

class Authentification(BaseModel):
    id_auth = AutoField()
    id_utilisateur = IntegerField()
    mot_de_passe = TextField()


class Operateurs(BaseModel):
    id_operateur = AutoField()
    nom_operateur = TextField()

class Villes(BaseModel):
    id_ville = AutoField()
    nom_ville = TextField()

class Contacts(BaseModel):
    id_contact = AutoField()
    id_borne = IntegerField(null=True)
    id_utilisateur = IntegerField()
    contact = TextField()

class Accessibilite(BaseModel):
    id_access = AutoField()
    horraire = TextField()

class Types_bornes(BaseModel):
    id_type = AutoField()
    type_borne = TextField()
    
class Bornes_imcompletes(BaseModel):
    id_incomplet = AutoField()
    id_borne = IntegerField()
    id_utilisateurs = ArrayField(null=True)
    com_ville = ArrayField(null=True)
    com_adresse = ArrayField(null=True)
    statut = TextField()

liste_borne = pd.read_excel('données_timer.xlsx')
del liste_borne['Unnamed: 0']
liste_borne = liste_borne.to_dict('records')

perf = [{'nombre_requete':3,'avec_procédure':0.008,'sans_procédure':0.015}
        ,{'nombre_requete':30,'avec_procédure':0.015,'sans_procédure':0.105},
        {'nombre_requete':330,'avec_procédure':0.167,'sans_procédure':0.859},
        {'nombre_requete':3330,'avec_procédure':1.718,'sans_procédure':8.329},
        {'nombre_requete':5291,'avec_procédure':3.509,'sans_procédure':15.033},
        ]
data_perf = pd.DataFrame(perf)

N = [3, 30, 330, 3300, 5291]
nom = ["avec_procédure", "sans_procédure"]
vitesse = [[0.008, 0.015],[0.015, 0.105],
           [0.167, 0.859],[1.718, 8.329],
           [3.509, 15.033]]

data_perf = pd.DataFrame(vitesse, columns = nom, index=N)
data_perf.plot(kind="line", logx = False, logy = False)
data_perf.plot.bar()


if __name__ == '__main__':

    creation_table = False
    test = False

    if creation_table:

        
        db.drop_tables([Utilisateurs,Bornes,Authentification,Operateurs,Villes
                        ,Contacts,Accessibilite,Types_bornes,
                        Bornes_imcompletes])
        
        db.create_tables([Utilisateurs,Bornes,Authentification,Operateurs,
                          Villes,Contacts,Accessibilite,Types_bornes,
                          Bornes_imcompletes])
        if test:
            
            nombre_requete = [0,3,33,333,3333]
            for i in range(0,len(nombre_requete)-1):
            
                  debut = time.time()
                  #for d in liste_borne[nombre_requete[i]:nombre_requete[i+1]]:
                  for d in liste_borne:
                      try:
                            """ integration des donnée dans la base avec un appel de la 
                            procédure stockée insertion crée sur pg admin l'interface 
                            graphique de postgres"""
                            
                            query = "call test_insertion('"+d['ville']+"','"+d['operateur']+"','"
                            query += d['accessibilité']+"', 'publique' ,'"+d['service']+"',"
                            query += str(d['lat'])+","+str(d['long'])+",'"+d['adresse']+"')"       
                            db.execute_sql(query)
            
                      except Exception as e:
                        print(e)
                  fin = time.time() - debut

                  print(f"""La fonction  met {fin} seconde a tourné
                        pour {nombre_requete[i+1]}""")
            
            for i in range(0,len(nombre_requete)-1):
               debut = time.time()
               
               #for d in liste_borne[nombre_requete[i]:nombre_requete[i+1]]:
               for d in liste_borne:
                   
                    id_borne = Bornes.select(fn.MAX(Bornes.id_borne)).scalar()
                    try:
                        id_borne = id_borne + 1
                    except:
                        id_borne = 1
                        
                    select_ville = Villes.select().where(Villes.nom_ville == d['ville'])
                    if len(select_ville) == 0:
                        ville = Villes.create(nom_ville = d['ville'])
                    else:
                        ville = select_ville[0]
                    
                    if ville == 'inconnue':
                        Bornes_imcompletes.create(id_borne=id_borne,statut = 
                                                  'a verifier')
                    select_access = Accessibilite.select().where(Accessibilite.horraire
                                                                == d['accessibilité'])
                    if len(select_access) == 0:
                        access =Accessibilite.create(horraire=d['accessibilité'])
                    else:
                        access = select_access[0]
                        
                    select_op = Operateurs.select().where(Operateurs.nom_operateur ==
                                                          d['operateur'])
                    if len(select_op) == 0:
                        op = Operateurs.create(nom_operateur = d['operateur'])
                    else:
                        op = select_op[0]
                    
                    Bornes.create(id_borne=id_borne,id_ville=ville.id_ville,
                                  id_operateur=op.id_operateur,id_access=
                                  access.id_access,id_type='1',service=d['service'],
                                  lat=d['lat'],long=d['long'],adresse=d['adresse'])
               fin = time.time() - debut
               print(f"""La fonction met {fin} seconde a tourné
                        pour {nombre_requete[i+1]}""")
                        
            