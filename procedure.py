#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Procédure stockées dans la base postgres sql

CREATE OR REPLACE PROCEDURE public.insertion(ville text, operateur text, "accessibilité" text, type_borne text, service text, lat real, "long" real, adresse text)
    LANGUAGE 'plpgsql'
    
AS $BODY$DECLARE
test INT;
ville1 text;
id_v integer;
id_b integer;
BEGIN
id_b= (select coalesce(id_borne) from (select max(id_borne) AS id_borne from bornes
							   )as id_b);
if id_b isnull then
id_b = 0;
end if;
id_b = id_b + 1;					
PERFORM ('*') FROM villes WHERE nom_ville = ville;
IF NOT FOUND THEN
INSERT into villes (nom_ville) values (ville) RETURNING ville INTO ville1;
else
id_v= (select coalesce(id_ville) from (select id_ville AS id_ville from villes
							   where(nom_ville=ville))as id_v);
END IF;
IF ville = 'inconnue' THEN
insert into bornes_imcompletes (id_borne,statut) values (id_b,'en attente');
End if;
END;
$BODY$;

@author: alexis
"""